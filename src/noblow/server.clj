(ns noblow.server
  (:require [noir.server :as server]
            [noir.core :as ncore]
            [noblow.views.common :as common])
  (:use [korma.core]
        [noblow.models]))


(server/load-views-ns 'noblow.views)

(ncore/defpage "/my-page" []
  (common/layout
    [:h1 "Welcome to my site!"]
    [:p "Hope you like it."]))


(ncore/defpage "/notions" []
  (common/layout
    [:div#welcome.post
    [:h1.title "notions"]
     [:div.content
      (for [x (select notions)]
        [:p (:notion  x)])]]
    [:div#example.post
     [:h2.title "examples"]
     [:div.content
      [:p "foo bar baz"]
      ]
     ]))

(defn -main [& m]
  (let [mode (keyword (or (first m) :dev))
        port (Integer. (get (System/getenv) "PORT" "3000"))]
    (server/start port {:mode mode
                        :ns 'noblow})))

