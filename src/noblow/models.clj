(ns noblow.models
  (:use [korma.db]
            [korma.core]))

(defdb sqll (sqlite3 {:host "sqlite3.db"}))


(defentity users
  (entity-fields :name :password))

(defentity notions
  (entity-fields :title :notion))

(has-many users notions)
(belongs-to notions users)

