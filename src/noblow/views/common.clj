(ns noblow.views.common
  (:use [noir.core :only [defpartial]]
        [hiccup.page :only [include-css html5 include-js]]
        [hiccup.element :only [javascript-tag]]))

(defpartial layout [& content]
  (html5
    [:head
     [:meta {:charset "utf-8"}]
     [:meta {:http-equiv "X-UA-Compatible" :content "IE=edge,chrome=1"}]
     [:title "noblow"]
     (include-css "/css/normalize.css")
     (include-css "/css/main.css")
     (include-css "/css/reset.css")
     (include-css "/css/default.css")
     (include-js "/js/vendor/modernizr-2.6.1.min.js")]
    [:body
     (include-js "//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js")
     (:script "window.jQuery || document.write('<script src=\"/js/vendor/jquery-1.8.0.min.js\"></script>')")
     (include-js "/js/plugins.js")
     (include-js "/js/main.js")
     [:div#wrapper
      [:div#header
       [:h1 [:a "noblow"]]]
      [:div#page
       [:div#content content]
       [:div#sidebar
        [:div#menu
         [:ul
          [:li.active [:a "Homepage"]]
          [:li [:a "About Us"]]
          ]]
        [:div#login.boxed
         [:h2.title "Client Account"]
         [:div.content
          [:form#form1
           "id"
           [:input {:type "text"}]
           "password"
           [:input {:type "text"}]
           ]
          ]
         ]
        [:div#updates.boxed
         [:h2.title "Recents"]
         [:div.content
          [:ul [:li [:h3 "today"] [:p "asdfasdfaf"]]]
          ]
         ]
        ]]]]))
